PROJECT_NAME = Reviews
SHELL := /bin/sh
help:
	@echo "Please use 'make <target>' where <target> is one of"
	@echo "  venv                     to create the virtualenv for the project"
	@echo "  requirements             install the requirements to the virtualenv"
	@echo "  db                       create the MySQL db for the project"
	@echo "  migrations               create migrations"
	@echo "  migrate                  run the migrations"
	@echo "  superuser                Create superuser with name superuser and password pass"
	@echo "  runserver                Start the django dev server"
	@echo "  - - -                    - - - - - - - - - - - - - - - - - - - - -"
	@echo "  create_messages          create locale messages"
	@echo "  compile_messages         compile locale messages"
	@echo "  - - -                    - - - - - - - - - - - - - - - - - - - - -"
	@echo "  test                     run tdd tests"

.PHONY: requirements

# Command variables
MANAGE_CMD = python manage.py
DJANGO_CMD = django-admin
PIP_CMD = pip
PIP_INSTALL_CMD = pip install
VIRTUALENV_NAME = venv

# The default server host local development
HOST ?= 0.0.0.0:8000

# -- Standalone Commands -------------------------------------------------------

venv:
	virtualenv --no-site-packages $(VIRTUALENV_NAME) # --always-copy

requirements:
	( \
		. $(VIRTUALENV_NAME)/bin/activate; \
		$(PIP_INSTALL_CMD) -r requirements.txt; \
	)

db:
	( \
		mysql -u root -p -e "drop database if exists bd_reviews;create database bd_reviews;"; \
	)

migrations:
	( \
		. $(VIRTUALENV_NAME)/bin/activate; \
		$(MANAGE_CMD) makemigrations; \
	)

migrate:
	( \
		. $(VIRTUALENV_NAME)/bin/activate; \
		$(MANAGE_CMD) migrate; \
	)

superuser:
	( \
		. $(VIRTUALENV_NAME)/bin/activate; \
		$(MANAGE_CMD) createsuperuser; \
	)

runserver:
	( \
		. $(VIRTUALENV_NAME)/bin/activate; \
		$(MANAGE_CMD) runserver $(HOST); \
	)

# -- Message Commands ----------------------------------------------------------

create_messages:
	( \
		. $(VIRTUALENV_NAME)/bin/activate; \
		cd core; \
		$(DJANGO_CMD) makemessages -l en -l es; \
	)

compile_messages:
	( \
		. $(VIRTUALENV_NAME)/bin/activate; \
		find core/ -name '*.mo' -exec rm -f {} \; ; \
		cd core; \
		$(DJANGO_CMD) compilemessages; \
	)

clear_messages:
	( \
		find core/ -name '*.mo' -exec rm -f {} \; ; \
	)

# -- Test Commands -------------------------------------------------------------

test:
# Run a local shell for debugging
	( \
		. $(VIRTUALENV_NAME)/bin/activate; \
		$(MANAGE_CMD) test $(module) --noinput --failfast; \
	)

# ------------------------------------------------------------------------------

requirements_freeze:
	( \
		. $(VIRTUALENV_NAME)/bin/activate; \
		$(PIP_CMD) freeze > requirements_freeze.txt; \
	)
